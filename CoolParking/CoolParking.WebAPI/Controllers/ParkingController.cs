﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            var balance = _parkingService.GetBalance();
            return Ok(balance);
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            var capacity = _parkingService.GetCapacity();
            return Ok(capacity);
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            var freePlaces = _parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}
