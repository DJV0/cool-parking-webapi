﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            return Ok(transactions);
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            string transactions;
            try
            {
                transactions = _parkingService.ReadFromLog();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return Ok(transactions);
        }
        
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> PutTopUpVehicle(TopUpVehicleModel input)
        {
            if(input == null)
            {
                return BadRequest("Input is null.");
            }
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            if (!Regex.IsMatch(input.Id, pattern))
            {
                return BadRequest("Wrong id! Pattern: AA-0000-AA.");
            }
            if(input.Sum < 0)
            {
                return BadRequest("Incorrect sum.");
            }

            try
            {
                _parkingService.TopUpVehicle(input.Id, input.Sum);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == input.Id);
            return Ok(vehicle);
        }
    }
}
