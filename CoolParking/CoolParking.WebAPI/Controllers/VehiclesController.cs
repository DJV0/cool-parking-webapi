﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(vehicles);
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            if (!Regex.IsMatch(id, pattern))
            {
                return BadRequest("Wrong id! Pattern: AA-0000-AA.");
            }
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);
            if(vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult<Vehicle> Post(VehicleModel inputVehicle)
        {
            if(inputVehicle == null)
            {
                return BadRequest("Vehicle is null.");
            }
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(inputVehicle.Id, inputVehicle.VehicleType, inputVehicle.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Created(new Uri("https://localhost:44339/api/vehicles"), vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult<Vehicle> Delete(string id)
        {
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            if (!Regex.IsMatch(id, pattern))
            {
                return BadRequest("Wrong id! Pattern: AA-0000-AA.");
            }
            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }
    }
}
