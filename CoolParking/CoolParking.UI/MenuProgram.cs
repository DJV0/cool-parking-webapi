﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.UI.Pages;
using EasyConsole;

namespace CoolParking.UI
{
    class MenuProgram : EasyConsole.Program
    {
        private List<Option> menuItems;

        public MenuProgram() : base("Cool Parking", breadcrumbHeader: true)
        {
            menuItems = new List<Option>()
            {
                new Option("Show parking balance", NavigateTo<ParkingBalancePage>),
                new Option("Show amount earned for current period (before logging)", NavigateTo<EarnedMoneyAmountPage>),
                new Option("Show parking places", NavigateTo<FreePlacesPage>),
                new Option("Show parking transactions for current period (before logging)", NavigateTo<CurrentParkingTransactionsPage>),
                new Option("Show transactions history (Transactions.log)", NavigateTo<TransactionsHistoryPage>),
                new Option("Show list of parking vehicles", NavigateTo<VehiclesInParkingPage>),
                new Option("Put vehicle in parking", NavigateTo<PutVehicleInParkingPage>),
                new Option("Pick up vehicle from parking", NavigateTo<PickUpVehicleFromParkingPage>),
                new Option("Top up vehicle balance", NavigateTo<TopUpVehiclePage>),
            };

            AddPage(new MainPage(this, menuItems));
            AddPage(new ParkingBalancePage(this));
            AddPage(new EarnedMoneyAmountPage(this));
            AddPage(new FreePlacesPage(this));
            AddPage(new CurrentParkingTransactionsPage(this));
            AddPage(new TransactionsHistoryPage(this));
            AddPage(new VehiclesInParkingPage(this));
            AddPage(new PutVehicleInParkingPage(this));
            AddPage(new PickUpVehicleFromParkingPage(this));
            AddPage(new TopUpVehiclePage(this));

            SetPage<MainPage>();
        }
    }
}
