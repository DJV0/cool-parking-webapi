﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.UI
{
    class Program
    {
        static Task Main(string[] args)
        {
            return new MenuProgram().Run(CancellationToken.None);
        }
    }
}
