﻿using CoolParking.BL.Services;
using CoolParking.UI.Models;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.UI.Pages
{
    class TopUpVehiclePage : Page
    {
        private HttpClient _client;
        public TopUpVehiclePage(EasyConsole.Program program) : base("Top up vehicle balance", program)
        {
            _client = new HttpClient();
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Top up vehicle balance:");
            var vehicleId = Input.ReadString("Enter vehicle id (AA-0000-AA):");
            var sum = await ObtainSum();
            TopUpVehicleModel vehicleModel = new TopUpVehicleModel() { Id = vehicleId, Sum = sum };
            var response = await _client.PutAsJsonAsync("https://localhost:44339/api/transactions/topUpVehicle",vehicleModel);
            if (response.IsSuccessStatusCode)
            {
                var vehicle = await response.Content.ReadFromJsonAsync<VehicleModel>();
                Output.WriteLine(vehicle.ToString());
            }
            else
            {
                Output.WriteLine($"Error: {await response.Content.ReadAsStringAsync()}");
            }

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }

        private async Task<decimal> ObtainSum()
        {
            decimal sum = 0;
            Output.DisplayPrompt("Enter sum:");
            decimal input = Convert.ToDecimal(Input.ReadInt());
            if(input > 0)
            {
                sum = input;
            }
            else
            {
                Output.DisplayPrompt("Incorrect sum.");
                input = await ObtainSum();
            }
            return sum;
        }
    }
}
