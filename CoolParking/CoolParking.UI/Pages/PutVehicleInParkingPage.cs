﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class PutVehicleInParkingPage : Page
    {
        private HttpClient _client;
        public PutVehicleInParkingPage(EasyConsole.Program program) : base("Put vehicle in parking", program)
        {
            _client = new HttpClient();
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Vehicle vehicle;
            Output.WriteLine("Put vehicle in parking:");
            vehicle = await ObtainInputVehicle(cancellationToken);
            var response = await _client.PostAsJsonAsync("https://localhost:44339/api/vehicles", vehicle);
            if (response.IsSuccessStatusCode)
            {
                Output.WriteLine("Vehicle has been parked.");
            }
            else
            {
                Output.WriteLine($"Error: {await response.Content.ReadAsStringAsync()}");
            }

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }

        private async Task<Vehicle> ObtainInputVehicle(CancellationToken cancellationToken)
        {
            Vehicle inputVehicle;
            var vehicleId = Input.ReadString("Enter vehicle id (AA-0000-AA):");
            var vehicleType = await Input.ReadEnum<VehicleType>("Choose vehicle type:", cancellationToken);
            Output.WriteLine("Enter vehicle balance:");
            var vehicleBalance = Convert.ToDecimal(Input.ReadInt());
            try
            {
                inputVehicle = new Vehicle(vehicleId, vehicleType, vehicleBalance);
            }
            catch (Exception e)
            {
                Output.WriteLine($"Incorrect inputs: {e.Message}");
                inputVehicle = await ObtainInputVehicle(cancellationToken);
            }
            return inputVehicle;
        }
    }
}
