﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class PickUpVehicleFromParkingPage : Page
    {
        private HttpClient _client;
        public PickUpVehicleFromParkingPage(EasyConsole.Program program) : base("Pick up vehicle from parking", program)
        {
            _client = new HttpClient();
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            var vehicleId = Input.ReadString("Enter vehicle id (AA-0000-AA):");
            var response = await _client.DeleteAsync($"https://localhost:44339/api/vehicles/{vehicleId}");
            if (response.IsSuccessStatusCode)
            {
                Output.WriteLine("You've picked up vehicle.");
            }
            else
            {
                Output.WriteLine($"Error: {await response.Content.ReadAsStringAsync()}");
            }

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
