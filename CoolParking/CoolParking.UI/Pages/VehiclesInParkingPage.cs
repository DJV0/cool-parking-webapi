﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using CoolParking.UI.Models;

namespace CoolParking.UI.Pages
{
    class VehiclesInParkingPage : Page
    {
        private HttpClient _client;
        public VehiclesInParkingPage(EasyConsole.Program program) : base("List of parking vehicles", program)
        {
            _client = new HttpClient();
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Parking vehicles:");
            var response = await _client.GetStringAsync("https://localhost:44339/api/vehicles");
            var vehicles = JsonConvert.DeserializeObject<List<VehicleModel>>(response);
            foreach (var vehicle in vehicles)
            {
                Output.WriteLine(vehicle.ToString());
            }

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
