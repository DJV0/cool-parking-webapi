﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.UI.Pages
{
    class FreePlacesPage : Page
    {
        private HttpClient _client;
        public FreePlacesPage(EasyConsole.Program program) : base("Parking places", program)
        {
            _client = new HttpClient();
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            var response1 = await _client.GetStringAsync("https://localhost:44339/api/parking/freePlaces");
            var freePlaces = JsonConvert.DeserializeObject<int>(response1);
            var response2 = await _client.GetStringAsync("https://localhost:44339/api/parking/capacity");
            var capacity = JsonConvert.DeserializeObject<int>(response2);

            Output.WriteLine("Places in parking:");
            Output.WriteLine($"-- free: {freePlaces}");
            Output.WriteLine($"-- occupied: {capacity - freePlaces}");

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
