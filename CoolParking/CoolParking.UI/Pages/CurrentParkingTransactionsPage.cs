﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using EasyConsole;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class CurrentParkingTransactionsPage : Page
    {
        private HttpClient _client;
        public CurrentParkingTransactionsPage(EasyConsole.Program program) : base("Transactions for current period (before logging)", program)
        {
            _client = new HttpClient();
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Transactions for current period:");
            var response = await _client.GetStringAsync("https://localhost:44339/api/transactions/last");
            var transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(response);
            foreach (TransactionInfo transaction in transactions)
            {
                Output.WriteLine(transaction.ToString());
            }

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
