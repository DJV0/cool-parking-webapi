﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.UI.Pages
{
    class TransactionsHistoryPage : Page
    {
        private HttpClient _client;
        public TransactionsHistoryPage(EasyConsole.Program program) : base("Transactions history (Transactions.log)", program)
        {
            _client = new HttpClient();
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Transactions history:");
            var response = await _client.GetStringAsync("https://localhost:44339/api/transactions/all");
            string transactions = JsonConvert.DeserializeObject<string>(response);
            Output.WriteLine(transactions);

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
