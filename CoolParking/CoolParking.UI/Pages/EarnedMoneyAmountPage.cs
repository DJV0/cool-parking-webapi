﻿using CoolParking.BL.Services;
using EasyConsole;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System;

namespace CoolParking.UI.Pages
{
    class EarnedMoneyAmountPage : Page
    {
        private HttpClient _client;
        public EarnedMoneyAmountPage(EasyConsole.Program program) : base("The parking's earned for current period (before logging)", program)
        {
            _client = new HttpClient();
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);
            decimal sum = 0;
            var response = await _client.GetStringAsync("https://localhost:44339/api/transactions/last");
            var transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(response);
            foreach (var transaction in transactions)
            {
                sum += transaction.Sum;
            }
            Output.WriteLine($"The parking's earned for current period: {sum}");

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}