﻿using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.UI;
using CoolParking.BL.Services;
using System.Net.Http;
using Newtonsoft.Json;

namespace CoolParking.UI.Pages
{
    class ParkingBalancePage : Page
    {
        private HttpClient _client;
        public ParkingBalancePage(EasyConsole.Program program) : base("Parking balance", program)
        {
            _client = new HttpClient();
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            var response = await _client.GetStringAsync("https://localhost:44339/api/parking/balance");
            var balance = JsonConvert.DeserializeObject<decimal>(response);
            Output.WriteLine($"Parking balance: {balance}");

            Input.ReadString("Press [Enter] to exit");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
