﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.UI.Models
{
    class VehicleModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Vehicle type: {VehicleType.ToString()}, Balance: {Balance}";
        }
    }
}
