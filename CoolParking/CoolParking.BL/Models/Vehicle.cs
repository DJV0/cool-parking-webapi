﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle()
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = VehicleType.PassengerCar;
            Balance = 0;
        }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            if(!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException("Wrong id! Pattern: AA-0000-AA.");
            }
            if(balance < 0)
            {
                throw new ArgumentException("Balance cant be less than zero.");
            }
            Id =  id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string plateNumber = "";
            for (int i = 0; i < 4; i++)
            {
                if (i == 2)
                {
                    plateNumber += '-';
                    for (int j = 0; j < 4; j++)
                    {
                        plateNumber += random.Next(0, 9);
                    }
                    plateNumber += '-';
                }
                plateNumber += (char)random.Next('A', 'Z');
            }
            return plateNumber;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Vehicle type: {VehicleType.ToString()}, Balance: {Balance}";
        }
    }
}