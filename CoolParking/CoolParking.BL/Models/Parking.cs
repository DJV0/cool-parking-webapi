﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking : IDisposable
    {
        private static Parking _parking;
        public decimal Balance { get; set; }
        public int Capacity { get; }
        public List<Vehicle> Vehicles { get; }
        public static Parking Instance
        {
            get
            {
                if (_parking == null)
                {
                    _parking = new Parking();
                }
                return _parking;
            }
        }

        private Parking()
        {
            Balance = Settings.Balance;
            Capacity = Settings.Capacity;
            Vehicles = new List<Vehicle>();
        }

        public void Dispose()
        {
            _parking = new Parking();
        }
    }
}