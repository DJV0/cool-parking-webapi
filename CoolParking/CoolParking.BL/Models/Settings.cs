﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { get; set; } = 0;
        public static int Capacity { get; set; } = 10;
        public static int ChargeInterval { get; set; } = 5;
        public static int LoggingInterval { get; set; } = 60;
        public static decimal PenaltyMultiplier { get; set; } = 2.5m;
        public static Dictionary<VehicleType, decimal> Tariff { get; } = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1}
        };
        public static string LogPath { get; set; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public static void AddTariff(VehicleType vehicle, decimal price)
        {
            if (Tariff.ContainsKey(vehicle))
            {
                Tariff[vehicle] = price;
            }
            else
            {
                Tariff.Add(vehicle, price);
            }
        }
    }
}
