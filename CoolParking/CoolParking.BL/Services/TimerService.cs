﻿using System;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(int interval)
        {
            if (interval<=0)
            {
                throw new ArgumentException("Interval should be more than zero.");
            }
            Interval = interval * 1000;
            timer = new Timer(Interval);
            timer.Elapsed += delegate { Elapsed?.Invoke(this,null); };
        }

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}