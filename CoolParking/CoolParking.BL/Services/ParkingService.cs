﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private List<TransactionInfo> _transactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.Instance;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _transactions = new List<TransactionInfo>();

            _withdrawTimer.Elapsed += MakePayment;
            _logTimer.Elapsed += WriteLog;

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        private void MakePayment(object sender, ElapsedEventArgs e)
        {
            if(_parking.Vehicles.Count == 0) { return; }
            decimal paymentTotal;
            foreach (var vehicle in _parking.Vehicles)
            {
                paymentTotal = CalculatePaymentTotal(vehicle);
                vehicle.Balance -= paymentTotal;
                _parking.Balance += paymentTotal;

                _transactions.Add(new TransactionInfo() { VehicleId = vehicle.Id, Time = DateTime.Now, Sum = paymentTotal });
            }
        }

        private decimal CalculatePaymentTotal(Vehicle vehicle)
        {
            decimal total = 0;
            decimal tariff = Settings.Tariff[vehicle.VehicleType];
            if (vehicle.Balance >= tariff)
            {
                total = tariff;
            }
            else
            {
                if (vehicle.Balance > 0)
                {
                    total = vehicle.Balance + (tariff - vehicle.Balance) * Settings.PenaltyMultiplier;
                }
                else
                {
                    total = tariff * Settings.PenaltyMultiplier;
                }
            }

            return total;
        }

        private void WriteLog(object sender, ElapsedEventArgs e)
        {
            // С этой проверкой ф-ция не проходит тест WhenLogTimerIsElapsed_ThenWriteLogIsHappened, но я считаю
            // что она нужна, чтобы лишний раз не дергать файл Transactions.log
            //if (_transactions.Count == 0)
            //{
            //    return;
            //}

            string logInfo = "";
            var tArray = _transactions.ToArray();
            _transactions.Clear();
            for (int i = 0; i < tArray.Length; i++)
            {
                logInfo += tArray[i].ToString() + "\n";
            }
            _logService.Write(logInfo);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(_parking.Vehicles.Count == _parking.Capacity)
            {
                throw new InvalidOperationException("Parking is full!");
            }
            if (_parking.Vehicles.Exists(v => v.Id == vehicle.Id))
            {
                throw new ArgumentException("Vehicle is already parked!");
            }
            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }
        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("VehicleId doesnt exist.");
            }
            if(vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Cant remove vehicle with debt.");
            }
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("VehicleId doesnt exist.");
            }
            if(sum < 0)
            {
                throw new ArgumentException("Incorrect sum.");
            }
            vehicle.Balance += sum;
        }
    }
}