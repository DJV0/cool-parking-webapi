﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File isnt found!");
            }
            try
            {
                using (var sr = new StreamReader(LogPath))
                {
                    string actual = sr.ReadToEnd();
                    return actual;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Write(string logInfo)
        {
            try
            {
                using (var sw = new StreamWriter(LogPath, true))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}